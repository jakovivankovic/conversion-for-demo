# import libraries
import folium
import pandas as pd
import utm

 
#Open document and conversion, lines starting with '#' are considered as comments
data = pd.read_csv('TB_testing.txt', sep=",", header=None, comment = '#')
data.columns = ["Number", "Time", "Identifier", "Length", "Data"]

#Filter data by ID
data_utm      = data[data['Identifier']=='000007F1'] #
data_gpsStamp = data[data['Identifier']=='00000721'] # GPS time stamp
data_zone     = data[data['Identifier']=='000007F3'] # from 1 to 60 zones

#Coordinates repartition
utm_x = data_utm.Data.str.slice(start=8)
utm_y = data_utm.Data.str.slice(start=0,stop=8)
zone  = data_zone.Data.str.slice(start=8)

#Reset the index, ex: 1,2,4,6 -> 0,1,2,3
utm_x = utm_x.reset_index(drop=True)
utm_y = utm_y.reset_index(drop=True)
zone = zone.reset_index(drop=True)

utm_y_mean = 0
utm_x_mean = 0
zone_mean = 0
for x in range(0,len(data_utm)):
    utm_x_mean = utm_x_mean + int(utm_x[x], 16)
    utm_y_mean = utm_y_mean + int(utm_y[x], 16)

for i in range(0,len(data_zone)):    
    zone_mean = zone_mean + int(zone[i], 16)

utm_x_temp = utm_x_mean / len(data_utm)
utm_y_temp = utm_y_mean / len(data_utm)
zone_mean  = zone_mean / len(data_zone)

temp_mean = utm.to_latlon(utm_x_temp,utm_y_temp, zone_mean, 'northern')
m = folium.Map(location=[temp_mean[0],temp_mean[1]],zoom_start=13)              #zoom_start max = 18

#Coordinate format convertion and map marker for every coordinate
for i in range(0, len(data_utm)-1, 2):                                          # The third argument jumps the numer of elements to print, this way we can reduce the number of printed markers
    temp = utm.to_latlon(int(utm_x[i], 16),int(utm_y[i], 16), 32, 'northern')   # the letter isn't needed for conversion 
    #temp = utm.to_latlon(359675.63617836125, 5183730.184975244, 32, 'T')
    folium.Marker(location= [temp[0], temp[1]], popup='I am over here').add_to(m)
m.save('index2.html')

#utm.from_latlon(51.2, 7.5)
#utm.to_latlon(Easting, Northing , 32, 'U')
#he = utm.to_latlon(359676, 5183730 , 32, 'U')









    
