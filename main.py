# import libraries
import folium
import utm
import pandas as pd
import time

 
#Open document and conversion, lines starting with '#' are considered as comments
data = pd.read_csv('NMEAtoUTM.txt', sep=",", header=None, comment = '#')
data.columns = ["X", "Y", "Z", "Time"]

#Filter data by ID
data_utm_x = data['X'] 
data_utm_y = data['Y'] 
data_time  = data['Time'] 

text_file = open("can_frames.txt", "w")
text_file.write("#TM4C1294 CAN BUS DATALOGGER\n")
text_file.write("#Message Number, Time, ID, DLC, DATA\n")
counter = 1
for i in range(1,len(data)):
    temp_string = "%d,%.4d,000007F1,%.2d,%.8x%.8x\n" %(counter,time.time(),8,int(float(data_utm_x[i])),int(float(data_utm_y[i])))
##    temp_string = "%.8d%.8d\n" %(int(data_utm_x[i]),int(data_utm_y[i]))
    text_file.write(temp_string)
    counter += 1
    temp_string = "%d,%.4d,000007F2,%.2d,%.16x\n" %(counter,time.time(),8,int(float(data_time[i])*100))   # because time = hhmmss.ss
    text_file.write(temp_string)
    counter += 1
    temp_string = "%d,%.4d,000007F3,%.2d,%.8x%.8x\n" %(counter,time.time(),8,0,int(32))
    text_file.write(temp_string)
    counter += 1
text_file.close()
